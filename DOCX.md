# Part 1: Code readability

```java
for (int i = 0; i <= 10; i += 2) {
    System.out.println(i);
}
```
```java
int[] x = new int[50];
for (int i = 0; i < 50; i++) {
    x[i] = i + 1;
}
```

# Part 2 : Method Review & Calling Methods
![img.png](screenshots/img.png)

> The error occurs because x is in the scope of the main function, not global, therefore another function cannot access it.

![img_1.png](screenshots/img_1.png)

> The value did not change because they are in different scopes.

![img_2.png](screenshots/img_2.png)

![img_3.png](screenshots/img_3.png)

![img_4.png](screenshots/img_4.png)

![img_5.png](screenshots/img_5.png)

![img_6.png](screenshots/img_6.png)

![img_7.png](screenshots/img_7.png)

![img_9.png](screenshots/img_9.png)

![img_10.png](screenshots/img_10.png)

![img_11.png](screenshots/img_11.png)

![img_12.png](screenshots/img_12.png)

![img_13.png](screenshots/img_13.png)

![img_14.png](screenshots/img_14.png)

# Part 3: Doing it on your own

![img_15.png](screenshots/img_15.png)

![img_16.png](screenshots/img_16.png)

![img_8.png](screenshots/img_8.png)