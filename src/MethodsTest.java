public class MethodsTest {
    public static void main(String[] args) {
        int x = 10;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);
        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x + 50);
        methodTwoInputNoReturn(1, 3.5);
        int z = methodNoInputReturnInt();
        System.out.println(z);
        System.out.println(sumSquareRoot(6, 3));
        String s1 = "hello", s2 = "goodbye";
        System.out.println(s1.length());
        System.out.println(s2.length());
        System.out.println(SecondClass.addOne(50));
        System.out.println(new SecondClass().addTwo(50));
    }

    public static void methodNoInputNoReturn() {
        int x = 50;
        System.out.println(x);
        System.out.println("I'm in a method that takes no input and returns nothing");
    }

    public static void methodOneInputNoReturn(int x) {
        System.out.println("Inside the method one input no return " + x);
    }

    public static void methodTwoInputNoReturn(int x, double d) {
        System.out.println("Inside the method two input no return " + x + " " + d);
    }

    public static int methodNoInputReturnInt() {
        return 6;
    }

    public static double sumSquareRoot(int a, int b) {
        return Math.sqrt(a + b);
    }
}
