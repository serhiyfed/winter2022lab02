import java.util.Scanner;

public class PartThree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(AreaComputations.areaSquare(sc.nextDouble()));
        System.out.println(new AreaComputations().areaRectangle(sc.nextDouble(), sc.nextDouble()));
    }
}
